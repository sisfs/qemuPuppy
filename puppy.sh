QEMU=/tmp/allinoneqemu_linux.$$
DIR="$(dirname $0)"

[ -n "$DIR" ] && cd "$DIR"

cp allinoneqemu_linux $QEMU
chmod +x $QEMU
trap "rm -f $QEMU" 0

[ "$(which qemu 2> /dev/null)" ] && {
  QEMU="$(which qemu)"

  echo Using $QEMU... 1>&2
}

#$QEMU -kernel vmlinuz -initrd initrd.gz -append root=/dev/ram0 -hda pup_save.2fs -hdb pup_214X.sfs -hdc devx_214X.sfs -hdd zdrv_214X.sfs -localtime -soundhw all -m 256 $*
#$QEMU -kernel vmlinuz -initrd initrd.gz -append root=/dev/ram0 -hda pup_save.2fs -hdb devx_214X.sfs -hdc pup_214X.sfs -hdd zdrv_214X.sfs -localtime -soundhw all -m 256 $*
$QEMU -kernel vmlinuz -initrd initrd.gz -append root=/dev/ram0 -hda pup_save.2fs -hdb pup_400.sfs -hdc devx_400.sfs -hdd zdrv_400.sfs -m 256 $*
