
----------------------------------------------------------------

QEMU-Puppy is an OS and a set of applications on a USB memory
stick. This OS can be booted natively, or on top of an other,
already installed, OS. Just borrow a PC, boot your own
environment and return the PC unaffected.

QEMU-Puppy can be used and distributed under the terms of the
GPL.

For more information, see
http://www.erikveen.dds.nl/qemupuppy/index.html

----------------------------------------------------------------

To start QEMU-Puppy on Windows:

 D:\> puppy.exe

Or simply double-click on puppy.exe.

----------------------------------------------------------------

To start QEMU-Puppy on linux:

 $ . ./puppy.sh

You have to source the script with ".", because the script
might not be executable as a result of the underlying VFAT
files system.

----------------------------------------------------------------

To install and start KQEMU on Windows:

Right click on "kqemu.inf" in Windows Explorer and choose
"Install".

In order to start kqemu, you must do:

 D:\> net start kqemu

----------------------------------------------------------------

To install and start KQEMU on Linux:

Download and install it, as described here:

http://fabrice.bellard.free.fr/qemu/kqemu-doc.html

(You have to compile the module against your own environment
and kernel. Pre-compiling it is no option.)

----------------------------------------------------------------

Information about Puppy-Linux can be found here:
http://www.goosee.com/puppy/flash-puppy.htm

Information about QEMU can be found here:
http://fabrice.bellard.free.fr/qemu/

Information about SysLinux can be found here:
http://syslinux.zytor.com/faq.php

----------------------------------------------------------------
