@echo off
echo.
echo Welcome to the QEMU Puppy Launcher for Windows!
echo.
echo This utility is licensed under the terms of the GNU General Public License,v2.
echo The license is in the COPYING file.
echo.
echo.
echo WARNING! 
echo.
echo    This utility is being offered with ABSOLUTELY no warranty, 
echo    expressed or implied.
echo.
echo USE IT ENTIRELY AT YOUR OWN RISK!
echo.
echo.
echo You can quit at any time by pressing CTRL+C.
echo.
pause
cls

:kqemu:
if exist c:\windows\system32\drivers\kqemu.sys (goto 2exist) else (goto 2no)
:2exist:
echo The KQEMU driver is installed on your computer.
echo QEMU will use it to speed things up a little.  ;-)
echo.
set n=1
pause
goto init
:2no:
echo The KQEMU driver is not installed on your computer.
echo KQEMU provides faster emulation in QEMU, so Puppy will run faster.
echo If you are thinking about running in graphical mode, install KQEMU.
echo.
echo PLEASE NOTE that you require administrator privileges to install it,
echo if this is not your own computer, then i advise you NOT to install it.
echo.
echo So, do you want to install it?
set /p m="Type: 1 and ENTER for YES, or 2 and ENTER for NO "
if %m% ==1 goto message
if %m% ==2 goto 2ino

:2ino:
set n=0
goto init

:message:
cls
echo.
echo.
echo To install KQEMU, you need to exit this utility,
echo and follow the two steps below.
echo.
echo (1) Right click on the file: kqemu.inf
echo     From the options in the menu that appears, select: Install
echo     That's all you need to do.
echo.
echo (2) You can then re-run this utility. (puppy.bat)
echo     KQEMU will be detected and (hopefully) used.
echo.
pause
goto exit

:init:
cls
net start kqemu
echo.
echo QEMU has been launched, 
echo using these parameters.
echo.
if %n% ==0 (
echo allinoneqemu.exe -kernel vmlinuz -initrd initrd.gz -append root=/dev/ram0 -hda pup_save.2fs -hdb pup_400.sfs -hdc devx_400.sfs -hdd zdrv_400.sfs -m 256
echo.
echo DO NOT CLOSE THIS WINDOW
allinoneqemu.exe -kernel vmlinuz -initrd initrd.gz -append root=/dev/ram0 -hda pup_save.2fs -hdb pup_400.sfs -hdc devx_400.sfs -hdd zdrv_400.sfs -m 256)
if %n% ==1 (
echo allinoneqemu.exe -kernel-kqemu -kernel vmlinuz -initrd initrd.gz -append root=/dev/ram0 -hda pup_save.2fs -hdb pup_400.sfs -hdc devx_400.sfs -hdd zdrv_400.sfs -m 256
echo.
echo DO NOT CLOSE THIS WINDOW
allinoneqemu.exe -kernel-kqemu -kernel vmlinuz -initrd initrd.gz -append root=/dev/ram0 -hda pup_save.2fs -hdb pup_400.sfs -hdc devx_400.sfs -hdd zdrv_400.sfs -m 256)
goto credits

:credits:
cls
echo.
echo QEMU has been stopped. 
echo It is now safe to close this window.
echo.
echo.
echo Credits:
echo.
echo 	Barry Kauler and the Puppy community for this great OS (puppylinux.org)
echo 	Fabrice Bellard for QEMU (www.nongnu.org/qemu)
echo 	Erik Veenstra for the now outdated QEMU-Puppy (erikveen.dds.nl)
echo 	Pendrive Linux for their scripts (pendrivelinux.org)
echo 	The GNU project for their General Public License (gnu.org)
echo 	Forum member AndyM3 for the original version of this file
echo 	Myself (CatDude) for cobbling this together.
echo.
echo.
echo.
echo Have a G'day cobbers!
echo.
pause
goto exit

:exiterr:
echo.
pause
goto exit

:exit:
