
This is basically the same package (qemu-puppy-2.17-1.tar.gz)
as provided by Erik Veenstra.

The main difference being it is now using:

	Puppy version 4.00 (k2.6.21.7)

Other changes include:

	1.	The way you run it in Windows. 
		There is no longer a puppy.exe file
		Use the puppy.bat file instead. (double click on it)
		
	2.	We are now using a pup_save.2fs and not a pup_save.3fs
		Likewise, the empty pup_save file is now called: pup_save.2fs.empty.gz
		So any instructions referring to pup_save.3fs files in the link below,
		should be substituted with pup_save.2fs
		
			http://www.erikveen.dds.nl/qemupuppy/index.html
			(I recommend that you visit and read the above page)
		
		That's about all the differences there are.


It is currently setup as follows:

	Keyboard and Language = uk qwerty (English)
	Timezone = London
	Video = Xvesa @ 800x600x16
	Using 256 MB of RAM
	
	I also edited the shutdown script (inside the pup_400.sfs),
	so that you have a choice of saving the session or not.
	
	
A word of CAUTION:
#####################################################################

		When running in QEMU mode (either in Linux or Windows)
		do not use the Menu option to: Shutdown > Reboot computer
		it DOES NOT work.
		
		It is best to use: Shutdown > Power-off computer

#####################################################################


Installation: 
( most of this is taken from eriks site: http://www.erikveen.dds.nl/qemupuppy/index.html#3.1.0 )

	1.	Copy all files from the qemu-puppy-4.00 directory 
		to the root of your VFAT/FAT32 formatted 256M (or more) USB memory stick.

		CatDude Tip:

			If you currently have a working USB memory stick (with syslinux),
			for example: you may have used the Puppy BootFlash installer to set it up.

			DO NOT copy the ldlinux.sys file from the qemu-puppy-4.00 directory onto the USB memory stick,
			as it will overwrite the one already there, and probably will not work.

		
	2.	Then do something like "syslinux /dev/sda1" (as root) or "D:\syslinux D:" to make your device bootable. 
		SysLinux puts ldlinux.sys on the device as well. 
		(See the SysLinux site and PuppyLinuxWiki in case you run into trouble... Don't ask me!) 
		[Note:1] (If you want to boot from USB natively.)

		Note:1
			The Windows version of SysLinux is included in QEMU-Puppy; 
			the Linux version isn't, since Linux usually comes with SysLinux.




#####################################################################

Notes:

During my testing, 
i decided to check the difference in bootup times to the desktop (in Windows XP),
between having the KQEMU accelerator installed and not having it installed.

The difference (on my test machine at least) was quite surprising

		Without KQEMU = 1 minute and 38 seconds
		With KQEMU    = 35 seconds

	FYI The test machine was a DELL Latitude D505 (1.7GHz, 512MB RAM)

If you decide to install the KQEMU accelerator (in Windows):

	Right click on the "kqemu.inf" file in Windows Explorer 
	and choose "Install".
	That's it.. 
	When you run the "puppy.bat" file, it will detect and use the KQEMU accelerator.


For more information about using/installing KQEMU
go and take a look at Erik's page: 

	http://www.erikveen.dds.nl/qemupuppy/index.html


CatDude
.
